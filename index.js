require('dotenv').config();
var express = require ('express');
var app = express();
var bodyParser = require ('body-parser');
var morgan = require('morgan');
var mongoose = require('mongoose');
var path = require('path')
var AWS = require("aws-sdk");
var Busboy = require('busboy');
var compression = require('compression')

// mongoose.connect('mongodb://localhost:27017/db');
mongoose.connect('mongodb://atwilliams88:'+process.env.DB_PASSWORD+'@ds155862.mlab.com:55862/utilityassistv1', { useNewUrlParser: true })
var cors = require('cors')

//Must Enable Cors to only our front end: Very Important Because User ID is stored in cookie and used to get customer info.
app.use(compression());//add this as the 1st middleware
app.use(cors());
app.use(morgan('tiny'));
app.use(bodyParser.urlencoded({ extended: false }))
app.use(bodyParser.json())

app.use(express.static(path.join(__dirname, 'build')));

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
  });

app.get('/admin', function (req, res) {
    res.sendFile(path.join(__dirname, 'build', 'index.html'));
});

app.get('/needs/*', cors(), function (req, res) {
  res.sendFile(path.join(__dirname, 'build', 'index.html'));
});  

//Routes
//Hardship API
var hardships = require('./routes/api/hardship')
app.use(hardships)

//Sign In API
var signin = require('./routes/api/signin')
app.use(signin)

//Sign UP API
var  signup = require('./routes/api/signup')
app.use(signup)

//Sign Out API
var signout = require('./routes/api/signout')
app.use(signout)

//Get Users By Id
var Users = require('./routes/api/users')
app.use(Users);

var Checkout = require('./routes/api/checkout')
app.use(Checkout)

var transactions = require('./routes/api/transactions')
app.use(transactions);

var ContactUs = require('./routes/api/contact')
app.use(ContactUs);

var Newsletter = require('./routes/api/newsletter')
app.use(Newsletter);

var TransactionGroup = require('./routes/api/transactionGroup')
app.use(TransactionGroup);

app.listen('3001' || process.env.port)
