var express = require('express');
var router = express.Router();
const User = require ('../../models/User');
var jwt = require('jsonwebtoken')

//Sign In Requires Email and Password

    router.post('/api/account/signin', (req,res)=>{
        //Check for Email and Password In Body
        if(!req.body.email) {
            return 
            res.send('email is required')
        }if (!req.body.password){
            return
            res.send('password is required')
        }
        //Save Body Objects to Global Variable
        var email = req.body.email;
        var password = req.body.password;
        
        //Search the User DB for a user with Email that was provided
        User.findOne({
            email: email,
        }, function (err, signedInUser){
            if (err) {
                return res.status(500).send({
                    success: false,
                    message: 'An Internal Error Occurred',
                    error: err
                })
            }
           if (!signedInUser){
               //Handle issues when to user exist with that email
                console.log(email +' does not exist')
                return res.status(404).send({
                    success: false,
                    message: 'email does not exist in system',
                });

                // Check that the password is correct 
            }if(signedInUser.validPassword(password)){
                return res.status(200).send({
                    success: true,
                    firstName: signedInUser.firstName,
                    lastName: signedInUser.lastName,
                    email: signedInUser.email,
                    loggedIn: true,
                    token: signedInUser._id,
                    // test: signedInUser,
                    Admin: signedInUser.Admin
                })
                // var user = {
                //     firstName: signedInUser.firstName,
                //     lastName: signedInUser.lastName,
                //     email: signedInUser.email,
                //     loggedIn: true,
                //     admin: signedInUser.Admin
                // }
                // jwt.sign({user}, "uaForMe", (err, token)=>{
                //     if(err) {
                //         console.log(err)
                //     } else {
                //         console.log(token)
                //         res.send({
                //             token
                //         })
                //         // res.send({
                //         //     token,
                //         // })
                //     }
                // })



            // }if (err) {
            //     console.log('Server Error')
            //     return res.status(500).send({
            //         message: 'Server Error '
            //     })
            // } 
            // else {
            //     console.log('password was not correct')
            //     res.status(404).send({
            //         message: 'password was not correct'
            //     })
            // }
            } else {
                res.status(406).send({
                    success: false,
                    message: "error please try again"
                })
            }
         })
    })

    // router.post('/admin', verifyToken, (req,res)=>{
    //     jwt.verify(req.token, 'uaForMe', (err, authData)=> {
    //         if (err){
    //             res.sendStatus(403)
    //         }else {
    //             res.send({
    //                 message: "Sign In",
    //                 authData
    //             })
    //         }
    //     })
    // })

    //verify Token
    // function verifyToken(req,res,next) {
    //     //get auth header value
    //     const bearerHeader = req.headers['authorization']
    //     //check if bearer is undefined
    //     if (typeof bearerHeader !== 'undefined') {
    //         const bearer = bearerHeader.split(" ")
    //         //get token from array
    //         const bearerToken = bearer[1];
    //         req.token = bearerToken;
    //         //next middleware
    //         next()
    //     }else {
    //         //Forbidden
    //         res.sendStatus(403)
    //     }
    // }

    router.post('/admin/token', (req,res)=> {
        var id = req.body.adminToken
        User.findOne ({
            _id: id
        }, function (err, user) {
            if (err) {
                res.send(err)
            }else {
                if(!user) {
                    res.sendStatus(403)
                } else if(!user.Admin) {
                    res.sendStatus(403)
                } else if(user.Admin === true ) {
                    res.send({
                        valid: true
                    })
                } else {
                    res.sendStatus(403)
                } 
            }
        })
    })

module.exports = router;