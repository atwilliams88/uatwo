var express = require('express');
var router = express.Router();
var UserSession = require('../../models/UserSession')

router.put('/api/account/signout/:userId', function(req,res){
    var userId = req.params.userId;
    UserSession.findOneAndUpdate({
        userId:userId
    },
    {
        Deleted: true
    }, function(err,doc){
        if (err) {
            console.log(err);
            res.status(500).send({
                success: false,
                message: "Internal Server Error"
            })
        }if (!doc){
            res.status(404).send({
                success: false,
                message: "No Session With That ID"
            })
        }else {
            res.status(200).send({
                success: true,
                message: "Signed Out",
                session: doc
            })
        }
    })
})

module.exports = router;