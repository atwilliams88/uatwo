var express = require ('express');
var router = express.Router();
var Hardship = require('../../models/Hardship');
var User = require('../../models/User');
var AWS = require('aws-sdk');
var s3 = new AWS.S3({params: {Bucket: "utilityassist"}});
var fs = require("fs");
const fileUpload = require('express-fileupload');

router.use(fileUpload());

    //Get All Hardships for a specific Known User
    router.get('/api/hardships/email/:email', function (req, res){
        email = req.params.email
        User.find({
            email:email
        })
        .populate("Hardships")
        .exec(function (err, doc){
            if(err){
                console.log(err)
                res.status(500).send({
                    success: false,
                    message: "internal server error"
                })

            }else {
                res.status(200).send({
                    success: true,
                    message: "successful request",
                    hardship: doc
                })
            }
        })
    })
    //Get all Hardships for an annonymous User
    router.get('/api/hardships/id/:id', function (req,res){
        //id is the Owner ID
        id=req.params.id
        Hardship.find({
            Owner: id
        })
        .populate("Owner")
        .exec(function (err, doc){
            if (err) {
                console.log(err);
                res.status(404).send({
                    success: false,
                    message: 'unable to locate hardship'
                })
            } else {
                res.status(200).send({
                    success: true,
                    message: 'Found User by ID',
                    hardship: doc
                })
            }
        })
    })

        //Get Hardship by Hardship ID
        router.get('/api/hardship/id/:id', function (req,res){
            //id is the Owner Hardship ID
            id=req.params.id
            Hardship.find({
                _id: id
            })
           
            .exec(function (err, doc){
                if (err) {
                    console.log(err);
                    res.status(404).send({
                        success: false,
                        message: 'unable to locate hardship'
                    })
                } else {
                    res.status(200).send({
                        success: true,
                        message: 'Found User by ID',
                        hardship: doc
                    })
                }
            })
        })
// todo donate directly to utility Assist
//todo show doc in edit hardship
// todo host
    //Get All Hardships that are approved
    router.get('/api/hardships', function (req,res){
        Hardship.find()
        .where('Status').equals("Approved")
        .gte('DueDate', Date.now())
        .limit(100)
        .sort('Balance')
        .populate("User")
        .exec(function (err, hardship){
            if (err){
                console.log(err)
                res.status(500).send({
                    success: false,
                    message: 'Internal Server Error'
                })
            } else {
                res.status(200).send({
                    success: true,
                    message: 'successful request',
                    hardship: hardship
                })
            }
        })
    })

        //Get All Hardships that are pending
        router.get('/api/hardships/pending', function (req,res){
            Hardship.find()
            // .where('Approved').equals(true)
            .where('Status').equals("Pending")
            .limit(100)
            .sort('Balance')
            .populate("User")
            .exec(function (err, hardship){
                if (err){
                    console.log(err)
                    res.status(500).send({
                        success: false,
                        message: 'Internal Server Error'
                    })
                } else {
                    res.status(200).send({
                        success: true,
                        message: 'successful request',
                        hardship: hardship
                        
                    })
                }
            })
        });
            //Get All Hardships that are Denied
            router.get('/api/hardships/DeniedStatus', function (req,res){
                Hardship.find()
                // .where('Approved').equals(true)
                .where('Status').equals("Denied")
                .limit(100)
                .sort('Balance')
                .populate("User")
                .exec(function (err, hardship){
                    if (err){
                        console.log(err)
                        res.status(500).send({
                            success: false,
                            message: 'Internal Server Error'
                        })
                    } else {
                        res.status(200).send({
                            success: true,
                            message: 'successful request',
                            hardship: hardship
                            
                        })
                    }
                })
            })
            //Get All Hardships that are Approved
            router.get('/api/hardships/ApprovedStatus', function (req,res){
                Hardship.find()
                // .where('Approved').equals(true)
                .where('Status').equals("Approved")
                .limit(100)
                .sort('Balance')
                .populate("User")
                .exec(function (err, hardship){
                    if (err){
                        console.log(err)
                        res.status(500).send({
                            success: false,
                            message: 'Internal Server Error'
                        })
                    } else {
                        res.status(200).send({
                            success: true,
                            message: 'successful request',
                            hardship: hardship
                            
                        })
                    }
                })
            })
    //Create a new Hardship
    router.post('/api/hardship', function (req,res){
        Owner = req.body.Owner,
        aLocation = req.body.Location,
        Description = req.body.Description,
        Balance = req.body.Balance,
        AccountNumber = req.body.AccountNumber
        Provider = req.body.Provider
        firstName = req.body.firstName
        lastName = req.body.lastName
        DueDate= req.body.dueDate
        FilterId= req.body.FilterId

        var newHardship = new Hardship()
        newHardship.Owner = Owner;
        newHardship.Location= aLocation
        newHardship.Description= Description;
        newHardship.Balance=Balance;
        newHardship.AccountNumber = AccountNumber;
        newHardship.Provider= Provider;
        newHardship.firstName = firstName;
        newHardship.lastname = lastName;
        newHardship.DueDate = DueDate;
        newHardship.FilterId = FilterId
        
        newHardship.save(function (err,Hardship){
            if (err){
                console.log(err);
                res.status(500).send({
                    success: false,
                    message: 'internal server error'
                })
            } else {
                console.log(Hardship)
                res.status(200).send({
                    success: true,
                    message: 'Your request has been recieved and will be reviewed in the next 72 Hours',
                    hardship: Hardship
                })
                User.findOneAndUpdate({
                    _id: Owner
                },{
                    Hardships: Hardship._id
                }, function(err, doc){
                    if(err) {
                        console.log(err)
                    } else {
                        console.log(doc)
                    }
                })

            }
        })
    })
    router.put('/api/hardships/deny', (req,res)=>{
        id = req.body.id
        Hardship.findById(id,function (err,hardship){
            if (err) throw err;
            hardship.set({Status: "Denied"});
            hardship.save(function (err, updatedHardship){
                if (err) throw err;
                res.send(updatedHardship)
            })
        })
    })

    router.put('/api/hardships/approve', (req,res)=>{
        id = req.body.id;
        Hardship.findById(id,function (err,hardship){
            if (err) throw err;
            hardship.set({Status: "Approved"});
            hardship.save(function (err, updatedHardship){
                if (err) throw err;
                res.send(updatedHardship)
            })
        })
    });

    router.put('/api/hardship/updateById', (req, res)=>{
        id=req.body.hardship;
        provider = req.body.provider;
        accountNumber = req.body.accountNumber;
        location= req.body.location;
        dueDate = req.body.dueDate;
        description = req.body.description;
        Hardship.findById(id,function (err,hardship){
            if (err) throw err;
            if (provider) {
            hardship.set({Provider: provider})
            }
            if(accountNumber) {
                hardship.set({AccountNumber: accountNumber})
            }
            if(location) {
                hardship.set({Location: location})
            }
            if(dueDate) {
                hardship.set({DueDate: dueDate})
            }
            if(description){
                hardship.set({Description: description})
            }
            hardship.save(function (err, updatedHardship){
                if (err) throw err;
                res.send(updatedHardship)
            })
        })
        });

    router.post ('/api/hardship/documents', (req,res)=> {
        console.log(req.files.sampleFile);
        fs.readFile("someString", function(err, data){
            var params = {
                Key: req.files.sampleFile.name,
                Body: req.files.sampleFile.data
            };
        s3.upload(params, function (err, data) {
                if (err) {
                    console.log(err)
                } else {
                    console.log("success")
                }
            })
        });
        res.end()
    });

module.exports = router;
