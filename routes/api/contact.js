var express = require('express');
var router = express.Router();
var sgMail = require('@sendgrid/mail');
sgMail.setApiKey(process.env.SENDGRID_API_KEY);


router.post('/contactUs', (req,res)=>{
    const msg = {
        to: 'sukiee_99@yahoo.com',
        from: req.body.email,
        subject: req.body.topic,
        text: req.body.description,
        html: '<h1>Email From Utility Assist User</h1>' + req.body.description,
      };
      sgMail.send(msg);
      res.send('sent')
});
module.exports = router;