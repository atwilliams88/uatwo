var express = require('express');
var router = express.Router();
const User = require ('../../models/User');
const bcrypt = require('bcrypt');
// Sign Up Page

    router.post('/api/account/signup', function (req, res, next) {
   
        // Simple Validation
        if (!req.body.firstName) {
            return res.status(500).send ({
                success: false,
                message: 'First Name Value is Required'
            })
        }
        if (!req.body.lastName) {
            return res.status(500).send ({
                success: false,
                message: 'Last Name Value is Required'
            })
        }
        if (!req.body.email) {
            return res.status(500).send ({
                success: false,
                message: 'Email is Required'
            })
        }
        if (!req.body.password) {
            return res.status(500).send ({
                success: false,
                message: 'Password is Required'
            })
        }
        email = req.body.email.toLowerCase()
        //Check if user email is already in DB
        User.find({
            email: req.body.email
        },(err, previousUser)=>{
            // Check for an Error
            if (err) {
                res.status(500).send('An error happened' + err)
            // Check to see if email is already in DB
            } if (!previousUser){
                console.log('Error this account is already registered')
                return res.status(404).send({
                    success: 'false',
                    message: 'This email address has already been used'
                })
            } else {
                // Save new User to DB
                var newUser = new User()
                newUser.firstName=req.body.firstName,
                newUser.lastName=req.body.lastName,
                newUser.email=req.body.email,
                newUser.password=newUser.generateHash(req.body.password)
                newUser.phone=req.body.phone,
                newUser.address=req.body.address

                newUser.save(function (err, user){
                    if(err) {
                        res.status(500).send('Error during save' + err)
                    } else {
                        console.log(user)
                        res.status(200).send(user)
                    }
                })
            }        
        })
    })
    

module.exports = router;