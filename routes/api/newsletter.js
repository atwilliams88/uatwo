var express = require('express')
var router = express.Router()
var Newsletter = require('../../models/NewsletterSubs')

router.post('/newsletter', (req,res)=> {
    var newSubscriber = new Newsletter();
    newSubscriber.email=req.body.email;
    newSubscriber.firstName=req.body.firstName;
    newSubscriber.lastName=req.body.lastName;
    newSubscriber.save((err,doc)=>{
        if(err) {
            console.log(err)
        } else {
            console.log(doc)
        }
    })
    res.send('new subscriber saved')
})


module.exports = router