var express = require ('express');
var router = express.Router();
var TransactionGroup = require ('../../models/transactionGroup.js');
var transactions = require('../../models/transactions');

router.get('/api/transactiongroup', (req,res)=> {
TransactionGroup.find()
    .where({processed:false})
    .exec(function (err, group){
        if (err) {
            console.log(err)
        }else {
            res.json({
                group
            })
        }
    });
});
router.post('/api/transactiongroup/processed/:id', (req,res)=>{
    id = req.params.id;
    console.log("id ", id);
   TransactionGroup.findOneAndUpdate({transactionGroupId:parseInt(id)},{processed:true},((err,group) => {
       if(err){
           console.log("group err", err)
       }else {
           console.log("group non error: ", group);
           transactions.updateMany({transactionGroupId: parseInt(id)}, {appliedToBill: true}, ((err,doc)=>{
               if (err){
                   console.log(err)
               }else {
                   console.log("group: ", group)
                   console.log("doc: ", doc)
                   res.status(200).send({
                       message:"success",
                       transaction: doc,
                       group: group
                   })
               }
           }))
       }
   })
)});

router.get("/api/unprocessed/groups/:id",(req,res)=> {
    id= req.params.id;
    TransactionGroup.find()
        .where({
            transactionGroupId: parseInt(id)
        })
        .exec((err,group)=>{
            console.log(group)
            if(err){
                console.log(err)
            }else {
                transactions.find()
                    .where({
                        transactionGroupId: parseInt(id)
                    })
                    .exec((err, doc)=>{
                        if(err){
                            console.log(err)
                        }else {
                            res.status(200).send({
                                message: "success",
                                matchingTransactions: doc,
                                matchingTg: group
                            })
                        }
                })
            }
    })
});


module.exports = router;