require('dotenv').config();
var express = require('express');
var router = express.Router();
var axios = require ('axios')
var stripe = require("stripe")(process.env.SK_LIVE);
var Transaction = require('../../models/transactions')
var Hardship = require('../../models/Hardship')
var Group = require('../../models/transactionGroup')
router.post('/save-stripe-token', (req,res)=> {
  res.send({
      id: req.body.id
  })
})

router.post('/save-data', (req,res)=> {
  source = req.body.source,
  amount = (req.body.amount)*100,
  hardships = req.body.hardships
  email = req.body.email
  currency = 'usd',
  stripe.charges.create({
    amount: amount,
    source: source,
    currency: currency,
    description: "Transaction Group: "+req.body.transactionGroupId,
    receipt_email: email
  })
  ////START Here Save Customers to Stripe Customers
  stripe.customers.create({
    // source: source,
    email: req.body.email
    
  }, function(err, customer) {
      if (err) throw err;
      else {
        console.log(customer)
      }
  });
  req.body.hardships.map((hardship)=>{
    var newTransaction = new Transaction()
    newTransaction.startingBalance = hardship.hardship.Balance;
    newTransaction.provider = hardship.hardship.Provider;
    newTransaction.accountNumber = hardship.hardship.AccountNumber;
    newTransaction.owner = hardship.hardship.Owner;
    newTransaction.moneyRaised = hardship.donation;
    newTransaction.needNumber=hardship.hardship._id
    newTransaction.transactionGroupId= req.body.transactionGroupId
    newTransaction.save(function (err, Transaction){
        if (err) {
          console.log(err)
        } else {
          console.log(Transaction)
        }
      })
      needNumber =hardship.hardship._id
      newMoney= hardship.donation

  })
  //get current value of money raised
  //this isnt working
  Hardship.findById(needNumber,function (err, hardship){
    if (err) throw err;
    currentMoneyRaised = hardship.MoneyRasied
  //sum current value of money raised and the new donation amount
    total = parseInt(currentMoneyRaised) + parseInt(newMoney)
  })
//add new total to Hardship
  Hardship.findById(needNumber,function (err,hardship){
      currentMoneyRaised = hardship.MoneyRasied
      total = parseInt(currentMoneyRaised) + parseInt(newMoney)
      if(err) throw err;
    hardship.set({MoneyRasied: total});
    hardship.save(function (err, updatedHardship){
      if (err) throw err;
      res.send(updatedHardship)
    })
  })
  //Here I need to update the money raised on hardship
  // res.send('saved')
})

router.post('/save-transaction', (req,res)=> {
    console.log("amount: ", req.body.amount)
  var newGroup = new Group()
  newGroup.transactionGroupId= req.body.transactionGroupId;
  newGroup.amount= req.body.amount;
  newGroup.donor = req.body.donor;
  
  newGroup.save(function(err, newGroup){
    if(err) {
      console.log(err)
    } else {
      console.log(newGroup)
      res.send('newGroup Created')
    }
  })
});
router.get("/api/getAllTransactions", (req,res)=> {
  Transaction.find({
      appliedToBill: false
  })
      .exec(function(err, transaction){
        if(err) {
          console.log(err)
        }else {
          res.send(transaction)
        }
      })
});
module.exports = router;