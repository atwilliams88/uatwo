var express = require('express');
var router = express.Router();
var User = require('../../models/User');

// //
// router.get('/api/account/users/:email', function (req,res){
//     email = req.params.email
//     User.findOne({
//         email:email
//     },function(err, doc){
//         if (err) {
//             console.log(err)
//         }else {
//             res.send({
//                 firstName: doc.firstName,
//                 lastName: doc.lastName,
//                 phone: doc.phone,
//                 hardships: doc.hardships,
//             })
//         }
//     })
// })

router.get('/api/account/token/:token', (req,res)=>{
    token = req.params.token;
    // User.findById(token, function (err,doc){
    //     if (err) {
    //         console.log(err)
    //     } else {
    //         res.send({User: doc})
    //     }
    // })
    User.findById()
        .where("_id").equals(token)
        .exec(function (err,doc) {
            if(err) {
                res.status(400).send({
                    success: false,
                    message: "not found"
                })} else{
                    res.status(200).send({
                        success: true,
                        message: "found user",
                        user: doc
                    })
                }
            })
});

module.exports = router;