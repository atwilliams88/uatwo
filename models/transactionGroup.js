var express = require('express');
var mongoose = require ('mongoose');
var Schema = mongoose.Schema;

var TransactionGroupSchema = new Schema ({
    transactionGroupId: {
        type: Number
    },
    amount: {
        type: Number
    },
    donor: {
        type: String
    },
    processed: {
        type: Boolean,
        default: false
    },
    transaction: {
        type: Schema.Types.ObjectId,
        ref: 'transactions'
    },
});
module.exports = mongoose.model('TransactionGroup', TransactionGroupSchema);