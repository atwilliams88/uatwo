const mongoose = require('mongoose');
const bcrypt = require('bcrypt');
var Schema = mongoose.Schema;
//creates mongoose type email
require('mongoose-type-email');

//Create User Schema
var UserSchema = new Schema ({
    //First name
    firstName: {
        type: String,
        default: '',
        required: true
    },
    //Last Name
    lastName: {
        type: String,
        default: '',
        required: true
    },
    // Email *** type is special NPM Package
    email: {
        type: mongoose.SchemaTypes.Email,
        // required: true,
        // unique: true,
    },
    phone:{
        type: String,
        required: false
    },
    address: {
        type: String
    },
    //Password
    password: {
        type: String,
        default: '',
        required: true
    },
    //Is Deleted Field
    isDeleted: {
        type: Boolean,
        default: false,
    },
    Hardships: {
        type: {type: Schema.Types.ObjectId, ref: 'Hardships'}
    },  
    Admin: {
        type: Boolean,
        default: false
    }  
});
//Will Hash Our Password
UserSchema.methods.generateHash = function (password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};
//Will Check Our Password
UserSchema.methods.validPassword = function (password) {
    return bcrypt.compareSync(password, this.password)
};

module.exports = mongoose.model('User', UserSchema)