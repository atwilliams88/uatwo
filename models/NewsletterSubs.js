var mongoose= require ('mongoose')
var Schema= mongoose.Schema

const NewsletterSchema = new Schema({
    letterId: {
        type: Schema.Types.ObjectId
    },
    email: {
        type: String,
    },
    firstName: {
        type:String,
    },
    lastName: {
        type:String,
    }
})

module.exports=mongoose.model('NewsletterSubs', NewsletterSchema)