var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var TransactionSchema = new Schema ({
    transactionDate: {
        type: Date,
        default: Date.now()
    },
    startingBalance: {
        type: Number,
    },
    provider: {
        type: String
    },
    accountNumber: {
        type: String
    },
    owner: {
        type: String,
    },
    moneyRaised: {
        type: Number,
    },
    appliedToBill: {
        type: Boolean,
        default: false
    },
    transactionGroupId: {
        type: Number
    },
    needNumber: {
        type: String
    },
    group: {
        type: Schema.Types.ObjectId,
        ref: 'TransactionGroups'
    }
});

module.exports = mongoose.model('transactions', TransactionSchema);