var mongoose = require('mongoose');
var moment = require('moment');
var Schema = mongoose.Schema

//
var HardshipSchema = new Schema ({
    Owner: { type: Schema.Types.ObjectId, ref: 'User' },
    Location: {
        type: String,
        default: '',
    },
    Description: {
        type: String,
        default: '',
    },
    DueDate: {
        type: Date,
        required: false,
        default: moment().add(30, 'days').calendar(),
    },
    Expired: {
        type: Boolean,
        default: false
    },
    Balance: {
        type: Number,
        default: 0,

    },
    AccountNumber: {
        type: String,
        default: '',
    },
    Provider: {
        //placeholder until we make the Provider DB
        type: String,
    },
    FirstName: {
        type: String,

    },
    LastName: {
        type: String,
    },
    Status: {
        type: String,
        default: 'Pending'
    },
    MoneyRasied: {
        type: Number,
        default: 0
    },
    FilterId: {
        type: Number,
        default:0
    }
});

module.exports = mongoose.model('Hardship', HardshipSchema)